package com.jlngls.classes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var casa1 = Casa()
        casa1.cor = "azul"


        var casa2 = Casa()
        casa2.cor = "amarela"
        casa2.abrirJanela()

        var casa3 = Casa()
        casa3.cor = "rosa"
    }

    class Casa{

        var cor : String = ""

     fun abrirJanela(){
         println("abrir janela")
     }
    }
}